<?php
/**
 * This file contains remote request method retrieve last inserted item for selected list.
 *
 * @package Freemius_Test
 *
 * @since 1.0.0
 */

namespace Freemius_Test\Admin;

/**
 * Define the Core_Admin_Remote_Request class.
 *
 * @since 1.0.0
 *
 * @package Freemius_Test
 */
class Core_Admin_Remote_Request {

	/**
	 * Initialize the Core class.
	 *
	 * @since 0.1.0
	 */
	public function init( $list_id ) {

		$response = wp_safe_remote_post( 'https://' . FREEMIUS_TEST_END_POINT . '/lists/get_lists.php', array(
				'method'  => 'POST',
				'timeout' => 45,
				'body'    => array( 'list_id' => $list_id )
			)
		);

		return $response['body'];
	}
}
