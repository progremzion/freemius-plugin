<?php
/**
 * This file contains the core plugin functions.
 *
 * @package Freemius_Test
 *
 * @since 1.0.0
 */

namespace Freemius_Test\Admin;

/**
 * Define the Core class.
 *
 * The Core class instance will hook the functions to WordPress actions and filters.
 *
 * @since 1.0.0
 *
 * @package Freemius_Test
 */
class Core_Admin {

	/**
	 * The option values
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $options The option values of this plugin
	 */
	private $options;

	/**
	 * Initialize the Core class.
	 *
	 * @since 1.0.0
	 */
	public function init() {
		// Admin menu
		add_action( 'admin_menu', array( $this, 'freemius_add_plugin_page' ) );

		// Admin settings initialization
		add_action( 'admin_init', array( $this, 'freemius_page_init' ) );

		// Admin notice bar to our plugin
		add_action( 'admin_notices', array( $this, 'freemius_add_settings_errors' ) );

	}

	/**
	 * Add admin menu to configure settings of the plugin
	 *
	 * @since 1.0.0
	 */
	public function freemius_add_plugin_page() {

		add_menu_page(
			'WP Test Menu',
			'WP Test Menu',
			'manage_options',
			'freemius-test',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Page callback
	 *
	 * @since 1.0.0
	 */
	public function create_admin_page() {
		$this->options = get_option( "freemius_test_option" );

		?>
        <div class="wrap">
            <form method="post" action="options.php">
				<?php
				settings_fields( 'freemius_test_option_group' );
				do_settings_sections( 'freemius_test' );
				submit_button();
				?>
            </form>
        </div>
		<?php
	}

	/**
	 * Register and add settings
	 *
	 * @since 1.0.0
	 */
	public function freemius_page_init() {
		register_setting(
			'freemius_test_option_group',
			'freemius_test_option',
			array( $this, 'sanitize' )
		);

		add_settings_section(
			'setting_section_id',
			__( 'WP Test Menu', 'freemius-test' ),
			'',
			'freemius_test'
		);

		add_settings_field(
			'item_lists',
			__( 'Select Lists', 'freemius-test' ),
			array( $this, 'freemius_test_item_lists' ),
			'freemius_test',
			'setting_section_id'
		);
	}

	/**
	 * Sanitize setting field as needed.
	 *
	 * Get the list last inserted item and store it in system to reduce multiple call
	 *
	 * @param string $input Values from form fields.
	 *
	 * @return array
	 * @since 1.0.0
	 *
	 */
	public function sanitize( $input ) {
		$new_input = array();

		if ( isset( $input['freemius_test_item_lists'] ) ) {

			$new_input['freemius_test_item_lists'] = sanitize_text_field( $input['freemius_test_item_lists'] );

			// Get the selected list's last inserted item
			// Bailout if value is not set
			if ( "" !== $input['freemius_test_item_lists'] ) {

				// Get the remote request instance
				$carr     = new Core_Admin_Remote_Request();
				$response = $carr->init( $new_input['freemius_test_item_lists'] );

				// Parse the response
				$parse_response = json_decode( $response );

				if ( isset( $parse_response->item_text ) ) {
					// Prepare message
					$message = __( 'Option updated! Post prepended text: ' . $parse_response->item_text );
					$type    = 'updated';

					// Update option value
					update_option( 'freemius_text', $parse_response->item_text );

				} else {
					// Prepare message
					$message = __( 'There was a problem. Try to select different option. Issue: ' . $parse_response->message );
					$type    = 'error';

					// Reset the value to avoid conflict
					update_option( 'freemius_text', "" );
				}

				// Show message to notice bar
				add_settings_error( 'freemius_test', 'freemius_test_option', $message, $type );
			} else {
				// Reset the value to avoid conflict
				update_option( 'freemius_text', "" );
			}
		}

		return $new_input;
	}

	/**
	 * Get the settings option array and print one of its values
	 *
	 * @since 1.0.0
	 */
	public function freemius_test_item_lists() {
		$value = ( isset( $this->options['freemius_test_item_lists'] ) ? $this->options['freemius_test_item_lists'] : '' ); ?>

        <select name="freemius_test_option[freemius_test_item_lists]">
            <option value="" <?php selected( $value, "" ); ?>>Select List</option>
            <option value="1" <?php selected( $value, "1" ); ?>>List1</option>
            <option value="2" <?php selected( $value, "2" ); ?>>List2</option>
            <option value="3" <?php selected( $value, "3" ); ?>>List3</option>
            <option value="4" <?php selected( $value, "4" ); ?>>List4</option>
        </select>
		<?php
	}

	/**
	 * Function will load all the custom error message based on the setting selected by user
	 *
	 * @since 1.0.0
	 */
	public function freemius_add_settings_errors() {
		settings_errors();
	}
}
