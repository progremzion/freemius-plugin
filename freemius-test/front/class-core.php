<?php
/**
 * This file contains the core plugin functions to update frontend.
 *
 * @package Freemius_Test
 *
 * @since 1.0.0
 */

namespace Freemius_Test\Front;

/**
 * Define the Core class.
 *
 * The Core class instance will hook the functions to WordPress actions and filters.
 *
 * @since 1.0.0
 *
 * @package Freemius_Test
 */
class Core {

	/**
	 * Initialize the Core class.
	 *
	 * @since 1.0.0
	 */
	public function init() {
		// Prepend the content
		add_filter( 'the_content', array( $this, 'filter_content' ), 10, 1 );
	}

	/**
	 * Prepend the content
	 *
	 * @since 1.0.0
	 */
	public function filter_content( $content ) {

		// Bail out if the Freemius setting isn't enabled
		if ( ! $this->is_setting_enabled() ) {
			return $content;
		}

		// Bail out if the value is not set due to some error
		if ( "" == get_option( 'freemius_text' ) ) {
			return $content;
		}

		// Prepend the item text and return the value
		return get_option( 'freemius_text' ) . $content;

	}

	/**
	 * Check whether the current post should display the Prepend text.
	 *
	 * @return bool True if the current post should display the Prepend text otherwise false.
	 * @since 1.0.0
	 */
	private function is_setting_enabled() {

		// Bail out if it is not single post
		if ( ! is_singular() ) {
			return false;
		}

		// Get global setting value and bail out if it is not set
		$global_setting_val = get_option( 'freemius_test_option' );

		// Bail out if option is not set
		if ( ! isset( $global_setting_val['freemius_test_item_lists'] ) ) {
			return false;
		}

		return true;
	}

}
