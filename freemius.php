<?php
/**
 * Plugin Name:     Freemius Test
 * Plugin URI:      https://freemius.com
 * Description:     Test Plugin to get the last inserted list item based on the setting value
 * Author:          Freemius <info@freemius.com>
 * Author URI:      https://freemius.com
 * Text Domain:     freemius-test
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         Freemius_Test
 */

// Backend
use Freemius_Test\Admin\Core_Admin;

// Frontend
use Freemius_Test\Front\Core;

/**
 * Define the base URL, useful to load scripts
 *
 * @since 1.0.0
 */
if ( ! defined( 'FT_URL' ) ) {
	define( 'FT_URL', plugin_dir_url( __FILE__ ) );
}

/**
 * Define end point constant, useful to retrieve third party lists
 *
 * @since 1.0.0
 */
if ( ! defined( 'FREEMIUS_TEST_END_POINT' ) ) {
	define( 'FREEMIUS_TEST_END_POINT', 'api.strikingweb.in' );
}

/**
 * Register our autoload routine for the `Freemius_Test` namespace.
 *
 * @throws Exception
 * @since 1.0.0
 */
function freemius_test_autoload() {

	spl_autoload_register( function ( $class_name ) {

		// Bail out if these are not our classes.
		if ( 0 !== strpos( $class_name, 'Freemius_Test\\' ) ) {
			return false;
		}

		$class_name_lc = strtolower( str_replace( '_', '-', $class_name ) );

		preg_match( '|^(?:(.*)\\\\)?(.+?)$|', $class_name_lc, $matches );

		$path = str_replace( '\\', DIRECTORY_SEPARATOR, $matches[1] );

		$file = 'class-' . $matches[2] . '.php';

		$full_path = plugin_dir_path( __FILE__ ) . $path . DIRECTORY_SEPARATOR . $file;

		if ( ! file_exists( $full_path ) ) {
			echo( "Class $class_name not found at $full_path." );

			return false;
		}

		require_once $full_path;

		return true;
	} );

}

/**
 * Autoloads classes.
 *
 * @since 1.0.0
 */
function ft_init() {

	// Auto load routine
	freemius_test_autoload();

	// Manage front view.
	$core = new Core();
	$core->init();

	// Manage Back-end.
	$ca = new Core_Admin();
	$ca->init();
}

add_action( 'init', 'ft_init' );
